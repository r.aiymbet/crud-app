<?php

    $pdo = new PDO('mysql:host=localhost;port=3306;dbname=products_db','root','');
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $search = $_GET['search'] ?? null;
    if ($search) {
        $statement = $pdo -> prepare('SELECT * FROM products WHERE title LIKE :title ORDER BY create_date DESC');
        $statement -> bindValue(':title', "%$search%");
    } else {
        $statement = $pdo -> prepare('SELECT * FROM products ORDER BY create_date DESC');
    }
    $statement -> execute();
    $products = $statement -> fetchAll(PDO::FETCH_ASSOC);

    /*echo '<pre>';
    var_dump($products);
    echo '</pre>'*/

    // http://localhost/CRUDapp/create.php?image=&title=Product+4&description=Description+for+Product+4&price=2700
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>Products</title>
</head>
<body>
    <h1 class="text-center m-5">Products list</h1>
    <div class="container">
        <p>
            <a href="create.php" class="btn btn-success">Create Product</a>
        </p>

        <div class="container">
            <form>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search" name="search" value="<?php echo $search ?>">
                    <button class="btn btn-outline-secondary" type="button">Search</button>
                </div>
            </form>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Create Date</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $i => $product): ?>
                <tr>
                    <th scope="row"><?php echo $i + 1 ?></th>
                    <td><img style="object-fit: cover" src="<?php echo $product['image'] ?>" alt="" border=3 height=80></td>
                    <td><?php echo $product['title'] ?></td>
                    <td><?php echo $product['description'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $product['create_date'] ?></td>
                    <td>
                        <a href="update.php?id=<?php echo $product['id'] ?>" type="button" class="btn btn-sm btn-primary">Edit</a>
                        <form style="display: inline-block" method="post" action="delete.php">
                            <input type="hidden" name="id" value="<?php echo $product['id']; ?>">
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                        <!--<a href="delete.php?id=<?php /*echo $product['id']; */?>" type="button" class="btn btn-sm btn-danger">Delete</a>-->
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div style="margin: 500px">

    </div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
-->
</body>
</html>
